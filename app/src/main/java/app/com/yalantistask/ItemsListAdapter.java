package app.com.yalantistask;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ItemsListAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private String[] items;
    private String[] names;
    private Context context;

    public ItemsListAdapter(Context context, String[] items, String[] names) {
        this.items = items;
        this.names = names;
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row, null);
        TextView textName = (TextView) vi.findViewById(R.id.ListName);
        TextView textItem = (TextView) vi.findViewById(R.id.ListItem);
        textName.setText(i == 5 ? "" : names[i]);
        textItem.setText(items[i]);
        return vi;
    }
}
