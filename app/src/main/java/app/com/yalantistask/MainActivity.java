package app.com.yalantistask;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
    private Resources res;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home ) {
            this.finishAffinity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res = getResources();
        String[] names = res.getStringArray(R.array.names);
        String[] items = res.getStringArray(R.array.data);
        String title = res.getString(R.string.app_name);
        ListView listview = (ListView) findViewById(R.id.detailsListView);
        if (listview != null) {
            listview.setAdapter(new ItemsListAdapter(this, items, names));
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        try {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            Log.e("Getting toolbar error", ex.getMessage());
        }

        Picasso.with(getApplicationContext()).load(R.drawable.hole1).into((ImageView)findViewById(R.id.imageViewLeft));
        Picasso.with(getApplicationContext()).load(R.drawable.hole2).into((ImageView)findViewById(R.id.imageViewRight));
    }

    public void onClick (View view) {
        String toastString = res.getString(R.string.pressed);
        Toast.makeText(this, toastString + view.getClass().getSimpleName(), Toast.LENGTH_LONG).show();
    }
}
